# Release notes

## Version 0.0.1

- added SKIP_CONTAINER_SECURITY_SCANS variable to disable containers security scanning
- fixed issue with Gitlab Id Tokens (CI_JOB_JWT deprecated)
- added DO_NOT_PUBLISH variable to docker-job-templates file to prevent docker image publishing
- added pre-commit framework support
- added some documentation (issue #7)

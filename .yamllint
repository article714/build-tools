---
extends: default

ignore: |
    .local
    .tox
    ./collections
    ./roles/ansible-role-systemd-networkd
    ./roles/geerlingguy.apache
    ./roles/ipr-cnrs.nftables
    ./roles/mrlesmithjr.mdadm
    ./roles/nginxinc.nginx
    ./tmp

rules:
    line-length:
        max: 150
        level: warning
        ignore: |
            .gitlab-ci.yml

    indentation:
        indent-sequences: consistent
        spaces: consistent
        check-multi-line-strings: false

    braces:
        max-spaces-inside: 1
        level: error

    brackets:
        max-spaces-inside: 1
        level: error

    comments:
        require-starting-space: true
        ignore-shebangs: true
        min-spaces-from-content: 1

    truthy:
        allowed-values:
            - "True"
            - "true"
            - "False"
            - "false"

# Tools used to build Article714 projects

This `build-tools` repository hosts tools and templates used by Gitlab [pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
for Article714 projects.

## Gitlab templates

Article714 projects are set to use Gitlab CI/CD features. In order to ease the deployment of CI
tools across all Article714 projects, and simplify `.gitlab-ci.yml` files, we provide a set of
templates:

* `ci/common-job-templates.yml`: utility jobs and jobs templates,
* `ci/docker-job-templates.yml`: jobs/templates for docker related tasks (image building, publishing,...),
* `ci/python-job-templates.yml`: jobs/templates related to [Python](https://python.org) development,
* `ci/container-image-matrix-pipeline.yml`: jobs/templates used to build multi-versions projects, i.e.
   projects that will deliver multiple versions of the same Docker image (depending for example on base image version)
* `ci/odoo-modules-pipeline.yml`: jobs/templates related to [Odoo](https://odoo.com) modules CI,
* `ci/testing-in-ovh-environment.yml` templates used to test code against an [OVHCloud](https://www.ovhcloud.com)
  infrastructure (see [Ansible714 project](https://gitlab.com/article714/ansible714)).

Those files are not actually [.gitlab-ci.yml templates](https://docs.gitlab.com/ee/ci/yaml/#includetemplate)
, but contain job definitions and *job templates*, i.e. job definitions whose names are
dot-prefixed (and thus [hided](https://docs.gitlab.com/ee/ci/jobs/#hide-jobs))
and can be used to defined other jobs using [extends keyword](https://docs.gitlab.com/ee/ci/yaml/index.html#extends).

*Each file should contain it own documentation*. In particular information about [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/)
that might be defined to act on jobs behaviour (e.g. `DO_NOT_PUBLISH` variable that prevents Docker image(s) to be published).

To use them in any Article714 project, you only have to include them at the top of `.gitlab-ci.yml` file:

```yaml

include:
    - project: "article714/build-tools"
      ref: master
      file: "ci/container-image-matrix-pipeline.yml"
    - project: article714/build-tools
      ref: master
      file: ci/testing-in-ovh-environment.yml
    - template: Security/SAST-IaC.latest.gitlab-ci.yml
    - template: Security/Secret-Detection.gitlab-ci.yml
```

For more details, see [Gitlab's official documentation](https://docs.gitlab.com/ee/ci/yaml/includes.html#gitlab-cicd-include-examples).

## Gitlab runners setup

Article714 project are automatically built using [Gitlab's CI/CD](https://docs.gitlab.com/ee/ci/)
 when a `.gitlab-ci.yml` is present in project directory.

Successful Pipelines execution depends on a properly configured set of [gitlab runners](https://docs.gitlab.com/runner/):

* at least one [Docker runner](https://docs.gitlab.com/runner/register/#docker), defined with a single tag: `docker`
* at least one [Shell linux runner](https://docs.gitlab.com/runner/register/#linux), defined with a single tag: `shell`
* at least one untagged `Docker` runner, used by Gitlab security scanners (eg. [IAC scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/))

Furthermore, the building host should also provide:

* a working [Docker](https://docs.docker.com/engine/install/) installation (cli + daemon)
* a [Python 3.x](https://www.python.org) interpreter, with [pip](https://pip.pypa.io/en/stable/)
